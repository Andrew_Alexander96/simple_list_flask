def Articles():
    articles = [
      {
       'id': 1,
       'title': 'Article One',
       'body': 'Lorem ipsum',
       'author': 'Andrew Alexander',
       'create_date': '07-25-2018'
      },
      {
       'id': 2,
       'title': 'Article Two',
       'body': 'Lorem ipsum',
       'author': 'Maria Anderson',
       'create_date': '07-25-2018'
       },
       {
        'id': 3,
        'title': 'Article Three',
        'body': 'Lorem ipsum',
        'author': 'Jonathan Swift',
        'create_date': '07-25-2018'
      },
    ]
    return articles
